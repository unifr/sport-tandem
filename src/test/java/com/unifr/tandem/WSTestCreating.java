package com.unifr.tandem;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.Base64;
import com.unifr.tandem.action.CyberCoach;
import com.unifr.tandem.domain.User;

import javax.ws.rs.core.MediaType;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 11/8/12
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class WSTestCreating {
    public static void main(String[] args) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(CyberCoach.BASE_URI);

        User userBean = new User();
        String passw = "st22st22";
        userBean.setUsername("st22");
        userBean.setPassword("st21st21");
        userBean.setEmail("st22@test.com");
        userBean.setRealname("st22 test");
        userBean.setPublicvisible("2");
        userBean.setUri(CyberCoach.REL_PATH + CyberCoach.RES_USERS + "/" + userBean.getUsername() + "/");
        String credentials = userBean.getUsername() + ":" + userBean.getPassword();

        service.
                path(CyberCoach.RES_USERS).
                path(userBean.getUsername()).
                header("Authorization", "Basic " + new String(Base64.encode(credentials))).
                accept(MediaType.APPLICATION_XML).      // set response content type
                type(MediaType.APPLICATION_XML).        // set request content type
                put(User.class, userBean);


    }
}

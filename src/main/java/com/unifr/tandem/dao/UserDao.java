package com.unifr.tandem.dao;

import com.unifr.tandem.domain.User;
import com.unifr.tandem.domain.UserPref;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 11/13/12
 * Time: 8:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserDao {

    private static Logger logger = Logger.getLogger(UserDao.class);

    protected JdbcTemplate jdbcTemplate = null;

    @Autowired
    public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public int getUserCount() {
        return this.jdbcTemplate.queryForInt("select count(0) from USER_PREFERENCES");
    }

    public User getUser(String username, String password) throws IncorrectResultSizeDataAccessException {
        User user = jdbcTemplate.queryForObject("select  username, realname, email from USER_PREFERENCES where username = ? AND password= ?",
                new Object[] { username, password },
                new RowMapper<User>() {

                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                        User user = new User();
                        user.setUsername(rs.getString("username"));
                        user.setRealname(rs.getString("realname"));
                        user.setEmail(rs.getString("email"));
                        return user;
                    }
                });
        return user;
    }

    public UserPref getUserPref(String username) {
        UserPref user = jdbcTemplate.queryForObject("select * " +
                "from USER_PREFERENCES where username = ?",
                new Object[] { username},
                new RowMapper<UserPref>() {

                    public UserPref mapRow(ResultSet rs, int rowNum) throws SQLException {
                        UserPref user = new UserPref();
                        user.setUsername(rs.getString("username"));
                        user.setRealname(rs.getString("realname"));
                        user.setEmail(rs.getString("email"));
                        user.setAge(rs.getInt("age"));
                        user.setFrequency(rs.getString("frequency"));
                        user.setCity(rs.getString("city"));
                        user.setSport(rs.getString("sport"));
                        user.setSportLevel(rs.getInt("sportLevel"));
                        return user;
                    }
                });
        return user;
    }

    public void addUser(User user) {

        logger.error("add user " + user.getUsername() + user.getRealname());

            jdbcTemplate.update(
                    "insert into USER_PREFERENCES (username, password, realname, email) values (?, ?, ?, ?)",
                    user.getUsername(), user.getPassword(), user.getRealname(), user.getEmail());

    }

    public void addUserPref(UserPref userPref) {
        jdbcTemplate.update(
                "update USER_PREFERENCES set sport = ?, sportLevel = ?, city = ?, sex = ?, age = ?, " +
                        "frequency = ?, comments = ? " + "where username = ?",
                userPref.getSport(), userPref.getSportLevel(), userPref.getCity(), userPref.getSex(),
                userPref.getAge(), userPref.getFrequency(), userPref.getComments(),
                userPref.getUsername());
    }

    public boolean hasUserPref(String username) {
        int number = jdbcTemplate.queryForInt(
                "select COUNT(sport) from USER_PREFERENCES where username = ?", username);
        return number > 0;
    }

    public List<UserPref> getSportPartners(UserPref userPref) {
        List<UserPref> users = new ArrayList<UserPref>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select username, realname, email, comments, phone from USER_PREFERENCES " +
                "where sport = ? and city = ? and frequency = ? " +
                "and sportLevel between ? and ? and age between ? and ? and username <> ?",
                userPref.getSport(), userPref.getCity(), userPref.getFrequency(), userPref.getSportLevel() - 1,
                userPref.getSportLevel() + 1, userPref.getAge() - 3, userPref.getAge() + 3, userPref.getUsername());
        for(Map row : rows) {
            UserPref user = new UserPref();
            user.setUsername((String)row.get("username"));
            user.setRealname((String) row.get("realname"));
            user.setEmail((String) row.get("email"));
            user.setPhone((String)row.get("phone"));
            user.setComments((String)row.get("comments"));
            users.add(user);
        }
        return users;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unifr.tandem.domain;

import com.unifr.tandem.domain.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LALA
 */
@XmlRootElement
public class Response {
    
    private String status;
    private List<Headline> headlinesItem  = new ArrayList<Headline>();

    @XmlElementRef
    @XmlElementWrapper(name = "headlines")
    public List<Headline> getHeadline() {
        return headlinesItem;
    }

    public void setHeadlines(List<Headline> headlines) {
        this.headlinesItem = headlines;
    }
    
    @XmlElement
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
   
    
    
}

package com.unifr.tandem.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 10/25/12
 * Time: 4:54 PM
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "link")
public class Link {
    private String description;
    private String href;

    @XmlAttribute
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlAttribute
    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}

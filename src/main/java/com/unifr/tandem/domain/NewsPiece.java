/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unifr.tandem.domain;

/**
 *
 * @author LALA
 */
public class NewsPiece {
    private String title;
    private String link;
    
    public NewsPiece() {
    }

    public NewsPiece(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
}

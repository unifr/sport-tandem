package com.unifr.tandem.action;

import com.opensymphony.xwork2.ActionSupport;
import com.unifr.tandem.domain.User;
import com.unifr.tandem.domain.UserPref;
import com.unifr.tandem.service.UserService;
import com.unifr.tandem.utils.ApplicationContextProvider;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 11/25/12
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class MakeFriend extends ActionSupport {

    private User userBean;

    @Override
    public String execute() throws Exception {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession(true);
        userBean = (User)session.getAttribute("user");

        ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
        UserService userService = (UserService)ctx.getBean("userService");

        boolean hasUserPref = userService.hasUserPref(userBean.getUsername());

        if(hasUserPref) {
            return "requestmade";
        } else {
            return SUCCESS;
        }

    }

    public User getUserBean() {
        return userBean;
    }

    public void setUserBean(User userBean) {
        this.userBean = userBean;
    }
}

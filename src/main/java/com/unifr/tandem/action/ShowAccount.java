package com.unifr.tandem.action;

import com.opensymphony.xwork2.ActionSupport;
import com.unifr.tandem.domain.User;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 11/29/12
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShowAccount extends ActionSupport{

    private User userBean;

    @Override
    public String execute() throws Exception {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession(true);
        userBean = (User)session.getAttribute("user");
        return SUCCESS;
    }

    public User getUserBean() {
        return userBean;
    }

    public void setUserBean(User userBean) {
        this.userBean = userBean;
    }
}

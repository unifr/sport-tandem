package com.unifr.tandem.action;

import com.opensymphony.xwork2.ActionSupport;
import com.unifr.tandem.domain.User;
import com.unifr.tandem.domain.UserPref;
import com.unifr.tandem.service.UserService;
import com.unifr.tandem.utils.ApplicationContextProvider;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 11/29/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class SearchFriends extends ActionSupport{

    private static Logger logger = Logger.getLogger(SearchFriends.class);

    private List<UserPref> friends;
    private User userBean;

    @Override
    public String execute() throws Exception {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession(true);
        User userBean = (User)session.getAttribute("user");

        ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
        UserService userService = (UserService)ctx.getBean("userService");

        UserPref userPref = userService.getUserPref(userBean.getUsername());
        friends = userService.getSportPartners(userPref);

        logger.debug("Number of friends:" + friends.size());

        return SUCCESS;
    }

    public List<UserPref> getFriends() {
        return friends;
    }

    public void setFriends(List<UserPref> friends) {
        this.friends = friends;
    }

    public User getUserBean() {
        return userBean;
    }

    public void setUserBean(User userBean) {
        this.userBean = userBean;
    }
}

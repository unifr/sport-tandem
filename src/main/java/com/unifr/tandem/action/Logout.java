package com.unifr.tandem.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 12/20/12
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class Logout extends ActionSupport {
    @Override
    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession(true);
        session.removeAttribute("user");
        return SUCCESS;
    }
}

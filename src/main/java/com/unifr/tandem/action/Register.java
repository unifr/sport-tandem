package com.unifr.tandem.action;

import com.opensymphony.xwork2.ActionSupport;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.unifr.tandem.domain.User;
import com.unifr.tandem.service.UserService;
import com.unifr.tandem.utils.ApplicationContextProvider;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

/**
 * Created with IntelliJ IDEA.
 * User: aliya
 * Date: 10/20/12
 * Time: 3:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class Register extends ActionSupport {
    private static Logger logger = Logger.getLogger(Register.class);

    private static final long serialVersionUID = 1L;

    private User userBean;


    @Override
    public String execute() throws Exception {

        //call Service class to store userBean's state with RESTWS

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(CyberCoach.BASE_URI);
        userBean.setPublicvisible("2");
        userBean.setUri(CyberCoach.REL_PATH + CyberCoach.RES_USERS + "/" + userBean.getUsername() + "/");

        try {
            service.
                    path(CyberCoach.RES_USERS).
                    path(userBean.getUsername()).
                    accept(MediaType.APPLICATION_XML).      // set response content type
                    type(MediaType.APPLICATION_XML).        // set request content type
                    put(User.class, userBean);
        } catch (Exception e) {
            logger.error("Can't put user in Cyber Coach");
        }

        //add user to local db
        ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
        UserService userService = (UserService)ctx.getBean("userService");
        userService.addUser(userBean);

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession(true);
        session.setAttribute("user", userBean);

        logger.debug("New user registered: " + userBean.getEmail());

        return SUCCESS;
    }

    public User getUserBean() {
        return userBean;
    }

    public void setUserBean(User user) {
        userBean = user;
    }


    @Override
    public void validate() {

        if (userBean.getRealname().length() == 0) {
            addFieldError("userBean.realname", "Full name is required.");
        }

        if (userBean.getUsername().length() == 0) {
            addFieldError("userBean.username", "First name is required.");
        }

        if (userBean.getEmail().length() == 0) {
            addFieldError("userBean.email", "Email is required.");
        }

        if (userBean.getPassword().length() == 0) {
            addFieldError("userBean.password", "Password is required");
        }

    }

}
